// $('.bookingForm__input_field-phone').inputmask("+7 (999) 999 99 99");

$('.numb').number_plugin({
    width: '100%', //ширина инпута на выводе (по умолчанию 65px редактируется как тут так и через css)
    height: '100%', //высота инпута на выходе (по умолчанию 35px редактируется как тут так и через css)
    negative: true, //включение поддержки отрицательных чисел (по умолчанию false)
    step: 1, //шаг прибавления и убавления (по умолчанию 1)
    animate: true, //включение анимации прибавления и вычитания (по умолчанию false)
    delay: 100, //задержка анимации между прибавлениями (по умолчанию 10ms)
    max: 100, //максимальное значение(по умолчанию false)
    min: 20, //минимальное значение(по умолчанию false)
});


//  $(document).ready(function () {

// $('form[id="form"]').validate({
//     rules: {
//         fname: 'required',
//         lname: 'required',
//         user_email: {
//             required: true,
//             email: true,
//         },
//         psword: {
//             required: true,
//             minlength: 8,
//         }
//     },
//     messages: {
//         fname: 'This field is required',
//         lname: 'This field is required',
//         user_email: 'Enter a valid email',
//         psword: {
//             minlength: 'Password must be atleast 8 characterslong'
//         }
//     },
//     submitHandler: function (form) {
//         form.submit();
//     }
// });

//  })

 $(document).ready(function () {
    // $(".bookingForm__select").select2({
    //     minimumResultsForSearch: Infinity
    // });
    $(".bookingForm__select").focus(function(event) {
        $(this).parent().addClass("bookingForm__select_active");
    });
    $(".bookingForm__select").focusout(function(event) {
        $(this).parent().removeClass("bookingForm__select_active");
    });
    
    $(".bookingForm__date").datepicker({
        multipleDates: false
    });

    

});


$(function () {

    $('.header__burger').on('click', function () {

    
        $('.menu').slideToggle(300, function () {
            if ($(this).css('display') === "none") {
                $(this).removeAttr('style');
            }
           
        });

    });

});


$('.nav__link').on('click', function () {

    $('#checkboxBurger').prop('checked', false);
    $('.header__nav .nav__list').hide()
})


$(document).ready(function () {
    $('#checkboxBurger').prop('checked', false)
});


        var swiper = new Swiper('.swiper1', {
            direction: 'vertical',
             autoHeight: true,
             spaceBetween: 20,
             initialSlide: 0,

            

          
            pagination: {
                el: '.swiper-pagination1',
                clickable: true,
            },
        });



//         $(document).ready(function () {
//  $('.mainBanner__content-info').addClass('animated fadeInLeft');
//   $('.mainBanner__content-info').css({
//               'opacity': '1'
//           })
//         });


        // $(window).scroll(function () {
        //     if ($(this).scrollTop() > $('.image__picture-wrapper').offset().top - 500) {
        //         $('.mainBanner__content-info').addClass('animated fadeInRight');
        //         // $('.image__picture-wrapper').css({
        //         //     'opacity': '1'
        //         // })
        //     }

        // });

var swiper2 = new Swiper('.swiper2', {

    slidesPerView: 3,
 spaceBetween: 25,

    slidesPerGroup: 3,
    
    loop: false,
    loopFillGroupWithBlank: true,
    pagination: {
        el: '.swiper-pagination2',
        clickable: true,
    },
    navigation: {
        nextEl: '.swiper-button-next',
        prevEl: '.swiper-button-prev',
    },

     breakpoints: {
         // when window width is <= 320px
        
         // when window width is <= 480px
        996: {
             slidesPerView: 2,
             spaceBetween: 25,
             slidesPerGroup: 2,
         },
         // when window width is <= 640px
         768: {
                //  width: 1000,
                 slidesPerGroup: 1,
                  slidesPerView: 1,
             spaceBetween:25,
         }
     }
});
$( function()
{
    $( 'a.photoGalleryTile__image-link' ).magnificPopup({
        type: 'image',
        closeOnContentClick: false,
        closeBtnInside: false,
        mainClass: 'mfp-with-zoom mfp-img-mobile',
        image: {
          verticalFit: true,
          titleSrc: function(item) {
            return item.el.attr('title');
          }
        },
        gallery: {
          enabled: true
        },
        zoom: {
          enabled: true,
          duration: 300, // don't foget to change the duration also in CSS
          opener: function(element) {
            return element.find('img');
          }
        }
      });
});